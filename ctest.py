from apidecorators.api import jwt_auth, get, insert, has_role, update, push, pull, \
                validate, get_many, delete, read_access, write_access, collection, aggregate, \
                update_array, get_from_array  
from apidecorators.fields import all_fields            
from cerberus import Validator

public = {'*': '*'}


schema_array = {
    'c': {'type': 'string', 'required': True}
}

schema = {
    'a': {'type': 'string'},
    'b': {'type': 'integer'},
    'nested': {
        'type': 'dict',
        'schema': {
            'n1': {'type': 'string'},
            'n2': {'type': 'integer'}
        }
    },
    'array': {
        'type': 'list',
        'schema': schema_array 
    }
}

validator = Validator(schema)
validator_array = Validator(schema_array)

def set_routes_test(routes):

    @routes.post('/api/test')
    @jwt_auth
    @collection('test')
    @write_access(public)
    @validate(validator=validator)
    @insert
    async def post_test(document, request, payload):
        return document   

    @routes.get('/api/test/{_id}')
    @jwt_auth
    @collection('test')
    @read_access({'__owner': all_fields(schema, without=['array', 'b']) - {'nested.n2'} | {'array.$.c'} })
    @get
    async def get_test(document, payload):    
        return document

    @routes.put('/api/test/{_id}')
    @jwt_auth
    @collection('test')
    @write_access({'__owner': '*'})
    @validate(update=True, validator=validator)
    @update
    async def put_test(old_doc, document, request, payload):      
        return document

    @routes.put('/api/test/{_id}/array')
    @jwt_auth
    @collection('test')
    @write_access({'__owner': '*'})
    @validate(validator=validator_array)
    @push('array')
    async def push_array(document, request, payload):
        document['__owner'] = payload['user']      
        return document

    @routes.get('/api/test/{_id}/array')
    @jwt_auth
    @collection('test')
    @read_access({'__owner': ['c']})
    @get_from_array('array')
    async def get_test(document, payload):   
        #the chance to remove empty objects in array   
        return document

    @routes.put('/api/test/{_id}/array/{sub_id}')
    @jwt_auth
    @collection('test')
    @write_access({'__owner': ['c']}, root='array')
    @update_array('array')
    async def update_test(document, payload):      
        return document

    @routes.get('/api/test/aggregate/agg1')
    @jwt_auth
    @collection('test')
    @read_access({'__owner': '*'})
    @get_many
    async def get_aggr_test(col, query, payload):  
        pipeline = [{"$match": {"b": 5}},
                    {"$project": {"__owner": 1, "length": {"$size": "$array"} }}
        ]
        return col.aggregate(pipeline)