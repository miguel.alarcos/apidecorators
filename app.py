import asyncio
from book import set_routes_book
from ctest import set_routes_test
from demanda import set_routes_demanda
from aiohttp import web
from apidecorators.api import cors_factory
from apidecorators.hooks import on_insert, on_update

@on_insert('book')
def on_insert_book(book):
    print('insert', book)


@on_update('book')
def on_update_book(book):
    print('update', book)

async def handle(loop):
    app = web.Application(loop=loop, middlewares=[cors_factory])
    routes = web.RouteTableDef()

    #set_routes_book(routes)
    #set_routes_test(routes)
    set_routes_demanda(routes)
    app.router.add_routes(routes)
    #app.router.add_static('/', './dist')
    await loop.create_server(app.make_handler(), '0.0.0.0', 8888)

def main():    
    loop = asyncio.get_event_loop()
    loop.run_until_complete(handle(loop))
    print("Server started at port 8888")
    loop.run_forever()
    loop.close()

if __name__ == '__main__':
    main()