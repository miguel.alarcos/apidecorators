from apidecorators.api import jwt_auth, get, insert, has_role, update, push, pull, \
                validate, get_many, delete, read_access, write_access, collection, aggregate, \
                update_array, get_from_array, public
from apidecorators.fields import all_fields            
from cerberus import Validator

s_presupuesto = {
    '_id': {'type': 'string'},
    'demandante': {'type': 'string'},
    'ofertante': {'type': 'string'},
    'descripcion': {'type': 'string', 'required': True},
    'cantidad': {'type': 'integer', 'coerce': int},
    'favorito': {'type': 'boolean'},
    'comentario': {'type': 'dict',
                'schema': {
                    "texto": {"type": "string"},
                    "fecha": {"type": "float"}
                }
    }
}

s_demanda = {
    '_id': {'type': 'string'},
    'demandante': {'type': 'string'},
    'descripcion': {'type': 'string', 'required': True},
    'lugar': {'type': 'string'},
    'presupuestos': {
        'type': 'list',
        'schema': s_presupuesto 
    }
}

v_demanda = Validator(s_demanda)
v_presupuesto = Validator(s_presupuesto)

def set_routes_demanda(routes):

    @routes.post('/api/demanda')
    @jwt_auth
    @collection('demanda')
    @write_access({'*': '*'})
    @validate(validator=v_demanda)
    @insert
    async def post_demanda(document, request, payload):
        document['demandante'] = payload['user']
        return document   

    @routes.get('/api/demanda/{_id}')
    @jwt_auth
    @collection('demanda')
    @read_access({'demandante': all_fields(s_demanda), '*': {'demandante', 'descripcion', 'lugar'}})
    @get
    async def get_demanda(document, payload):    
        return document

    @routes.put('/api/demanda/{_id}')
    @jwt_auth
    @collection('demanda')
    @write_access({'demandante': {'descripcion', 'lugar'}})
    @validate(update=True, validator=v_demanda)
    @update
    async def put_demanda(old_doc, document, request, payload):      
        return document

    @routes.put('/api/demanda/{_id}/presupuestos')
    @jwt_auth
    @collection('demanda')
    @write_access({'*': {'descripcion', 'cantidad'}})
    @validate(validator=v_presupuesto)
    @push('presupuestos')
    async def push_presupuesto(old_doc, document, request, payload):
        document['ofertante'] = payload['user']
        document['demandante'] = old_doc['demandante']      
        return document

    @routes.get('/api/demanda/{_id}/presupuestos')
    @jwt_auth
    @collection('demanda')
    @read_access({'ofertante': {'descripcion', 'cantidad'}})
    @get_from_array('presupuestos')
    async def get_presupuestos(document, payload):   
        #the chance to remove empty objects in array   
        return document

    @routes.put('/api/demanda/{_id}/presupuestos/{sub_id}')
    @jwt_auth
    @collection('demanda')
    @write_access({'ofertante': {'descripcion', 'cantidad'}, 'demandante': {'favorito', 'comentario'}}, root='presupuestos')
    @validate(update=True, validator=v_presupuesto)
    @update_array('presupuestos')
    async def update_presupuesto(old_doc, document, payload):      
        return document

    @routes.get('/api/demanda/agregado/agg1')
    @public
    @collection('demanda')
    @read_access({'*': '*'})
    @get_many
    async def get_aggr_demanda(col, query, payload):  
        pipeline = [
                    {"$project": {"demandante": 1, "cuantos_presupuestos": {"$size": "$presupuestos"} }}
        ]
        return col.aggregate(pipeline)
    
    @routes.get('/api/demanda/agregado/comentarios')
    @public
    @collection('demanda')
    @aggregate
    async def get_aggr_comentarios(col, query, payload):  
        ofertante = query["ofertante"]
        pipeline = [
    {"$match": {"presupuestos.ofertante": ofertante}},
    {"$unwind": "$presupuestos"},
    {"$match": {"presupuestos.ofertante": ofertante}},
    {"$group": {"_id": "$presupuestos.ofertante", "comentarios": {"$push": {
        "texto": "$presupuestos.comentario.texto",
        "fecha": "$presupuestos.comentario.fecha",
        "autor": "$demandante"
        }}}}
    ]
        return col.aggregate(pipeline)