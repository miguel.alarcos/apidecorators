from apidecorators.api import jwt_auth, get, insert, has_role, update, push, pull, \
                validate, get_many, delete, read_access, write_access, collection, aggregate, \
                update_array, get_from_array              
from cerberus import Validator

public = {'*': '*'}

schema = {
    'author': {'type': 'string'},
    'title': {'type': 'string'},
    'root': {'type': 'string'},
    'stars': {'type': 'integer'}
}

validator = Validator(schema)

schema_comments = {
    'author': {'type': 'string'},
    'message': {'type': 'string'}
}

validator_comments = Validator(schema_comments)

def set_routes_book(routes):

    @routes.get('/api/book/aggregate')
    @jwt_auth
    @collection('book')
    @aggregate
    async def get_aggr_stars(col, query, payload):  
        author = query["author"]
        pipeline = [{"$project": {"author": 1, "stars": 1}},
                    {"$match": {"author": author}}, 
                    {"$group": {"_id": "$author", "total_stars": {"$sum": "$stars"}}}]
        return col.aggregate(pipeline)

    @routes.get('/api/book')
    @jwt_auth
    @collection('book')
    @read_access(public)
    @get_many
    async def get_many_book(col, query, payload):  
        author = query["author"]
        return col.find({"author": author}).skip(0).limit(10)

    @routes.delete('/api/book/{_id}')
    @jwt_auth
    @collection('book')    
    @delete # TODO
    async def delete_book(query):  
        pass

    @routes.get('/api/book/{_id}')
    @jwt_auth
    @collection('book')
    @read_access({'__owner': ['title']})
    @get
    async def get_book(document, payload):      
        return document

    @routes.put('/api/book/{_id}/push/comments')
    @jwt_auth
    @collection('book')
    @write_access({'__owner': ['*'], 'root': ['author']})
    @validate(validator=validator_comments)
    @push('comments')
    async def handle_push(document, request, payload):      
        return document

    # TODO permissions, quizas con un argumento array de usuarios en @pull
    @routes.put('/api/book/{_id}/pull/{pull}/{sub_id}')
    @jwt_auth
    @pull
    async def handle_pull(document, request, payload):      
        pass

    @routes.post('/api/book')
    @jwt_auth
    @collection('book')
    @write_access(public)
    @validate(validator=validator)
    @insert
    async def handle_post(document, request, payload):
        return document       
    
    @routes.put('/api/book/{_id}')
    @jwt_auth
    @collection('book')
    @write_access({'__owner': ['*'], 'root': ['author']})
    @validate(update=True, validator=validator)
    @update
    async def handle_put(old_doc, document, request, payload):      
        return document    

    @routes.put('/api/book/{_id}/comments/{sub_id}')
    @jwt_auth
    @collection('book')
    @write_access({'__owner': ['*'], 'root': ['author']})
    @validate(update=True, validator=validator_comments)
    @update_array('comments')
    async def handle_push(document, request, payload):      
        return document

    