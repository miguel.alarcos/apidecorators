from apidecorators.api import pick, pick_from_array

def test_pick_array_basic():
    array = [{'a': 1, 'b': 2}, {'a': 0, 'b': 3}]
    ret = pick_from_array(array, ['b'])

    assert ret == [{'b': 2}, {'b': 3}]

def test_pick_array_missing_field():
    array = [{'a': 1, 'b': 2}, {'a': 0}]
    ret = pick_from_array(array, ['b'])

    assert ret == [{'b': 2}, {}]

def test_pick_basic_star():
    doc = {'a': 1, 'b': 2}
    ret = pick(doc, {'*': '*'}, 'mike')

    assert ret == doc

def test_pick_star_user_in_permissions():
    doc = {'a': 1, 'b': 2, '__owner': 'mike'}
    ret = pick(doc, {'__owner': '*'}, 'mike')

    assert ret == doc

def test_pick_star_user_not_in_permissions():
    doc = {'a': 1, 'b': 2, '__owner': 'miguel'}
    ret = pick(doc, {'__owner': '*'}, 'mike')

    assert ret == {}

def test_pick_fields():
    doc = {'a': 1, 'b': 2, '__owner': 'mike'}
    ret = pick(doc, {'__owner': ['a', 'c']}, 'mike')

    assert ret == {'a': 1}

def test_pick_dollar():
    doc = {'a': 1, 'b': 2, 'c': [{'n1': 'aaa'}], '__owner': 'mike'}
    ret = pick(doc, {'__owner': ['c.$.n1']}, 'mike')

    assert ret == {'c': [{'n1': 'aaa'}]}