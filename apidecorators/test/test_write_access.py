from unittest.mock import MagicMock, Mock, patch
from apidecorators.api import write_access
import apidecorators
import pytest
from asyncio import Future
from aiohttp import web # add to requirements of tests

def getitem4find_one(data):
    def helper(name):
        aux = MagicMock()
        fut = Future()
        fut.set_result(data)
        aux.find_one = MagicMock(return_value=fut)
        return aux 
    return helper

request = MagicMock()
request.match_info.get = MagicMock(return_value="5be07b16ce441003952d6653")

future = Future()
future.set_result({"a": 1, "b": 2})
request.json = MagicMock(return_value=future)

@pytest.mark.asyncio    
async def test_write_access():

    m = MagicMock()

    decorator = write_access({'user': ['a']})
    @decorator
    async def f(col, old_doc, doc, request, payload):
        m(col, old_doc, doc, request, payload)

    with patch('apidecorators.api.db') as mock_db:
        mock_db.__getitem__.side_effect = getitem4find_one({'user': 'miguel'})
        await f('test', request, {'user': 'miguel'})
        m.assert_called_with('test', {'user': 'miguel'}, {"a": 1}, request, {'user': 'miguel'})

@pytest.mark.asyncio    
async def test_write_star():

    m = MagicMock()

    decorator = write_access({'user': '*'})
    @decorator
    async def f(col, old_doc, doc, request, payload):
        m(col, old_doc, doc, request, payload)

    with patch('apidecorators.api.db') as mock_db:
        mock_db.__getitem__.side_effect = getitem4find_one({'user': 'miguel'})
        await f('test', request, {'user': 'miguel'})
        m.assert_called_with('test', {'user': 'miguel'}, {"a": 1, "b": 2}, request, {'user': 'miguel'})

@pytest.mark.asyncio    
async def test_write_no_access():
    
    decorator = write_access({'user': '*'})
    @decorator
    async def f(col, old_doc, doc, request, payload):
        m(col, old_doc, doc, request, payload)

    with patch('apidecorators.api.db') as mock_db:
        mock_db.__getitem__.side_effect = getitem4find_one({'user': 'miguel'})
        res = await f('test', request, {'user': 'mike'})
        assert res.body == b'{"error": "not authorized"}'

@pytest.mark.asyncio    
async def test_every_body_can():

    m = MagicMock()

    decorator = write_access({'*': '*'})
    @decorator
    async def f(col, old_doc, doc, request, payload):
        m(col, old_doc, doc, request, payload)

    with patch('apidecorators.api.db') as mock_db:
        mock_db.__getitem__.side_effect = getitem4find_one({'user': 'miguel'})
        await f('test', request, {'user': 'mike'})
        m.assert_called_with('test', {'user': 'miguel'}, {"a": 1, "b": 2}, request, {'user': 'mike'})
