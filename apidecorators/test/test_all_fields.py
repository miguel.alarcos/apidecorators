from apidecorators.fields import all_fields

schema_array = {
    'c': {'type': 'string', 'required': True}
}

schema = {
    'a': {'type': 'string'},
    'b': {'type': 'integer'},
    'nested': {
        'type': 'dict',
        'schema': {
            'n1': {'type': 'string'},
            'n2': {'type': 'integer'}
        }
    },
    'array': {
        'type': 'list',
        'schema': schema_array 
    }
}

def test_basic():
    ret = all_fields(schema)
    assert ret == set(['a', 'b', 'nested.n1', 'nested.n2', 'array.$.c'])

def test_basic_without():
    ret = all_fields(schema, without=['array'])
    assert ret == set(['a', 'b', 'nested.n1', 'nested.n2'])

def test_basic_without_and_minus():
    ret = all_fields(schema, without=['array']) - {'nested.n1'}
    assert ret == set(['a', 'b', 'nested.n2'])

def test_basic_without_and_plus():
    ret = all_fields(schema, without=['array']) | {'array.$.c'}
    assert ret == set(['a', 'b', 'nested.n1', 'nested.n2', 'array.$.c'])


