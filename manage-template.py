import os

project = 
name = 
user = 
password = 
userdeploy = 
passworddeploy = 
commit = 

def handle_build_push():
    cmd = f"docker build -t registry.gitlab.com/miguel.alarcos/{project}/{name}:{commit} ."
    os.system(cmd)
    cmd = f"docker login registry.gitlab.com -u $user -p $password && docker push registry.gitlab.com/miguel.alarcos/{project}/{name}:{commit}"
    os.system(cmd)

def down_and_handle_upload_docker_compose():
    handle_down()
    cmd = 'scp -i /home/miguel/default.pem docker-compose-prod.yml root@test.hispadev.net:/root/docker-compose.yml'

    os.system(cmd)

def handle_status():
    cmd = 'ssh -i /home/miguel/default.pem root@test.hispadev.net "docker-compose ps"'

    os.system(cmd)

def handle_up():
    cmd = f'ssh -i /home/miguel/default.pem root@test.hispadev.net \
          "docker login registry.gitlab.com -u {userdeploy} -p {passworddeploy} && \
          docker-compose up -d"'

    os.system(cmd)

def handle_down():
    cmd = 'ssh -i /home/miguel/default.pem root@test.hispadev.net \
          "docker-compose down"'

    os.system(cmd)

def main():
    while True:
        print("1 - build & push")
        print("2 - status")
        print("3 - down & upload docker-compose-prod.yml")
        print("4 - up")

        print("0 - exit")

        option = input("select: ")
        if option == '1':
            handle_build_push()
        elif option == '2':
            handle_status()
        elif option == '3':
            down_and_handle_upload_docker_compose()
        elif option == '4':
            handle_up()    
        elif option == '0':
            break

if __name__ == '__main__':
    main()